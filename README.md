# Lutherhaus-Netzwerk

(NixOS-)Konfiguration für gemanagte Networkkomponenten.


## Geräte

- Unifi Controller (https://unifi.luhj.de)
- Unifi Security Gateway (usg.luhj.de)


## Passwörter

siehe [Trac](https://intra.luhj.de/trac/)


# VLANs und Adressbereiche

## mgm (Management)

Aktive Netzwerkkomponenten: Controller, Router, Switches, APs

- VLAN: untagged
- IPv4: 10.2.1.0/24
- IPv6: 2001:470:70a0:4901::/64

## int (Internal)

Büro, Rymatzkis, Telefon, Mitarbeiter

- VLAN-ID 2
- IPv4: 10.2.2.0/24
- IPv6: 2001:470:70a0:4902::/64
- SSID: INTERN @ Lutherhaus

## gue (Guests)

Öffentlicher Internetzugang für Besucher

- VLAN-ID 3
- IPv4: 10.3.0.0/16
- IPv6: 2001:470:70a0:4903::/64
- SSID: GAST @ Lutherhaus

## sl (Sound+Light)

Mischpult, Projektion, Licht

- VLAN-ID 4
- IPv4: 10.2.4.0/24
- IPv6: 2001:470:70a0:4904::/64
- SSID: SOUND+LIGHT @ Lutherhaus


# Racks

ID | Ort
-- | ---
MDF | Heizungsraum
IDF-0-0 | Flur Jörgs Büro/Sitzungszimmer
IDF-0-1 | Marcos Keller
IDF-1-0 | Gemeindebüro
IDF-1-1 | Technikraum (Sakristei)
IDF-2-0 | Christophs Büro
