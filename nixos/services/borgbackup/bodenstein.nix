{ pkgs, lib, ... }:
let
  home = "/zfs/backup/borg";
in
{
  config = {
    services.borgbackup.repos = {
      musculus = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCjoQPR6k/WE2dhnzAppMwLS5etvR/roAXyTs81NNu0EJY037bwHOjTUykB7xKwm5khVs2pFhi/oZLltufeM3trmSmITv6u3BRcUbUdQyndEyMO0y65wpoYFdE9pqYkkjiJLMjGsa1j/urIDxBCI5xtFYV8LM7qKcIz/jhQnJQarsYgYCu8Ej0hdCjNo63Sl0QfO/4/DVq2vD7hWqeAeOKQfRacBWDZTaAOsZFm3GAJYA0wPK1zSO73IlEghwxUxx8UtHHwmQeap8lTQeIJeMLRdQr+IE2Z43B7OyY3lqfpGRaOdAtexUyTRpfldRJHg4mUH2ekqsB38kzneK284I7fZJxZXhYSvumNiDRkrsbgKl7vfmYeCp4LFqVm8gDPWsenv9z7QLPP/8f776iPEELLQ+SDpkWdew+c3Bka/jxqvojT3NBnOce2zgpUaEAowsU+CQF/KaFSm4mbbq6w6EW57TDJEEy7hO1dckp2aKSLOJcbdhXJ+PnSU4keOEt/pQf8KlsPHewUUKXYT9xxDU7YdubV5QnaUbQqsLy2CGVGyYsf1oknUq3XG4WQUKSYQ2l9rnhI61qrrg7inO0qE6gBC8I6PZI+0d7q3ysdwxVrlQMzRqMB6x2d8zw0LrqoVGZvnnNqwaBqOEYlsRayYR+uKXKBVOyV0COc1c1AAX5JrQ== root@musculus" ] ;
        path = "${home}/musculus" ;
        allowSubRepos = true;
      };
      sl4 = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCo1XsFdZkI/Gc0MYQOVcCCZvmckYABOkHlLM2GsbrXvMqneYiE8t9gV617za+yzwxRmr5mjGcB6vIKImW2Kfn7p9kFmIwOKc31PaezVTfujuM+rZiheV1tltNXO0rkGVho8kdOa3QiEUO8z4RUcDtXejhy7J0C4N2L0tJgUJfhtqhyT+KUUhZFgF+UpEblnVbcTJleH7y8YAv9idEvDz07ZdZeZUW6M1TFcoiFQlk920/K4rW/aXG5Geoz3gmZI17822epVTKCY9XLDQ1lKlPaINhcS++wZKyE/zTeuuQP1AO1t5mGqJa18bL75josjWeDVx0/HZo8t1ksCwAkoTWNN7VbCEM7DD2Ti/v8Hix4X9eWWsy/FnU27fmt05tNNl5KYtiCBT2wCjGXTlI2QNmAbz/6tK9/zgYJLGbmOaTg4arxdBNuOtcOYVSXEov5dEex2YV6g5Iqi+L60SYsB6pw8MLrrKEL+QvsIyfdfjhf+ahXd1VnMzvSynYsJy9DAck= root@sl4" ] ;
        path = "${home}/sl4";
        allowSubRepos = true;
      };
      aio = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6V/Sxq3WmVcu0BoXMYWgRQL9gMuIE7SQHQRIUNAEkBObKm3sdCyqpyLtG2AvQos1thzVzheHTZpegsEJXQITVHlzq1Lxe2bgBTo30uN3JZVfhR2yS6v87n9LjQITl27r+T9ZSdHVRF7BwdHHgabpZMx8lsPbwvX39KaJCrkQEVWuNOw08pwFxbbd0ULk/ePTPOnCTSFrB0Maw/gPLWR/l4U03h5M1Vy/XaJk90LWv2+iDP7VZ3w7lYO/AUOEtjQxPyAUuup+22W65wc7s4MZOjYHFc/PfD9bI80G6vp86P+qSggCD/4UL1YAxbk1adRvWaAtqszGbWWrUEr8ThgvkrY3qmj2w3mxV89V+VEs1mT0sbXvnZ5vJ7iwXEsRZHS4ObluL6ITnfQRZeNfPuaRQiuRgFauhTzkF8tV0ZVmTAlEUu+FSBZFj4WOcWK3NGALGcxRD2gPUd4zhWEzRx7OFJBHDtSTcoMjc3NNYbvKRVfH6M5ancS6b43Iv62987S0= root@aio" ];
        path = "${home}/aio";
        allowSubRepos = true;
      };
      hyperius = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC9wenNEeY8RPGF6x/hUhsSBmuqye9G8hgkAghUjJnPENiML6cR7ldlStyjH+/pBCXUcv+FDgkKP1kRZCHGnYHn/1OhZT/BXewHvGix69otFPTZQ0PBs01VpBn10e5UY+Ed5O/yfGjEOIqDjPZHT0DL7AQ5/DH3YrPYXuWPVZIEFzKi7S8kuZzyZ8spWhz3J679y6+E+SvVum+yxkF7nsYXJcEqISosV3lkYxMFlTAzhfpozmJESkQXjRjZ4fx+ugNOy+/6zAwhsiNHB6aSkg31b7xi9tLZhcfINheru0WlPvyuG2zQQzC05/yu/22IQUBUJYrZ7HIzAG1d38eWl9AZFhub7eyaPQta81todd/zLsQww10WLYGExn90FTShtI0rw88JXvS4LiL9Gy+frDGa3sI7lhpDvSE+XPyBvDZTFSwyRZZG3lRu86dJVJ/kelJV/OtgtW8K8By8zaaMPatIMDmYEnCNpzXH/hgs1lNVjTTX71B1Mnb+9nB8aTJKksk= root@hyperius" ];
        path = "${home}/hyperius";
        allowSubRepos = true;
      };
      melanchton = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDal0QiEcKGWJlQEX1sykAuofEqSvUXxKvsHOo4rUOGU6nV48oxmgPzeBFzUrEE833QEMbUHLTQx59swf93eFTrHLIfPS8cDmMA5US9e9mZA7mJX7JpUi2fJpQ6Q2ORFlt99nw38x3yheeyiSo6lP9buGqhr4efGPpDE++mKROBRsQeORPCI1Xaw11OPQPoD5zbgSIu1l7G923kn0FKXdzqXcugTXbi/yDmRLcb4+/L3ZLjbPQsixTBWGQ1XnyK0P53rV+wGsSHnhGMZvWbydolyBnXhtXXb5E+z1BSLi4/kamVqzpVzYcGVWlDJT8OpXzmQenJI6tz+3jV2G2hSnTug5rGc4Bzv6+pRVhxXcTA9wLPAwAzZdNIfMpp3pkJ4zfsPBbOMs++kiDsyTclqlwKWtKW6E9JBHAspM49RiGoTqLFN4OJ8aZWkKkqF1xNteLS0Bmu2QI8TSuFUiY9BY7as38BpvhRQzqw115oSpRA12xI1xZXeNnCDHT3LVdUa38= root@melanchton" ];
        path = "${home}/melanchton";
        allowSubRepos = true;
      };
      calvin = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCqf4C7yNwS8NPty99BlbTcYC5fSEjAx3GmroGk333CB7QNBoFsLtbTNRHuLEwhf3kIt0ERXfWj5A/zED2nGHTGJn5irOGHUe1qTe5Pj5dstuuXdKwbM5Hoig6AK0wtS/zVfjpLAkwcS2f+OWes95T0KLiWbtPreWgfZtkIRiIequYxvDAGWvOD2u2wlQJNuUSciSJqJsLkSd/cMMEQudrqHruVzx1IjUbI5Z9rXnI1zdM+mToLFNlXvyQ6HDRIped28v2dWDk/JusMMRi7Fw2XolPuAbDrRVTLn3Gu6mkFTkGCcQkgznv/kwx4QaJI4oOBc4qHpnux06I8Dhp5Hq/Lmcp1TOAWgI9AkmbELb07ClFnpCcqPS9tBF3prqFvQtgQf05EbflgqehxwTep44c6J8sq97leChpczpS/IoniZQwfZqIWJIcutLyGsg87L+spcgSZ9i/gQKwmqwz/MoNDLRnwmZlmFemXtdZ4d6zp4+/s6yiszakLJ1tDjLpGqLM= root@calvin" ];
        path = "${home}/calvin";
        allowSubRepos = true;
      };
    };

    security.sudo.extraRules = [ {
      users = [ "borg" ];
      commands = [
        { command = "/run/wrappers/bin/mount"; options =  [ "NOPASSWD" ]; }
        { command = "/run/wrappers/bin/umount"; options =  [ "NOPASSWD" ]; }
      ];
    } ];

    services.borgmatic = {
      enable = true;
      settings = {
        location = {
          source_directories = [ "/mnt/lightshark" ];
          repositories = [ "${home}/lightshark/repo" ];
          one_file_system = true;
        };
        storage = {
          encryption_passcommand = "cat ${home}/lightshark/encryption_passphrase";
        };
        retention = {
          keep_daily = 10;
          keep_weekly = 6;
          keep_monthly = 18;
          keep_yearly = 5;
        };
        consistency = {
          checks = [ "repository" ];
        };
        hooks.before_actions = [ "${home}/lightshark/pre.sh" ];
        hooks.after_actions = [ "${home}/lightshark/post.sh" ];
      };
    };

    systemd.services.borgmatic-ls1 = {
      serviceConfig = {
        User = "borg";
        ProtectSystem = "yes";
      };
      script = "borgmatic -v1";
      path = [ pkgs.borgmatic "/run/wrappers" ];
      startAt = [ "Sun,Tue,Fri 19:44:07" ];
    };

    systemd.timers.borgmatic.enable = false;

    environment.systemPackages = with pkgs; [
      borgmatic
      cifs-utils
    ];

    fileSystems."/zfs/backup" = {
      device = "zfs/backup";
      fsType = "zfs";
    };

    users.users.borg = {
      inherit home;
      createHome = true;
      isSystemUser = true;
      group = "borg";
    };

    users.groups.borg = {};
  };
}
