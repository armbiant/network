{ pkgs, lib, config, ... }:

let
  home = "/var/lib/borg";

  stdExcludes = builtins.toFile "borgbackup.exclude" ''
    .cache/
    /tmp/*
    /var/cache/*
    /var/tmp/*
  '';

in
{
  config = {
    environment.systemPackages = with pkgs; [ borgbackup ];

    services.borgmatic = {
      enable = true;
      settings = {
        location = {
          source_directories = [];  # needs to be overriden locally
          repositories = [ "ssh://borg@bodenstein.luhj.de/./repo" ];
          one_file_system = true;
          exclude_from = [ stdExcludes ];
          exclude_caches = true;
          exclude_if_present = [ ".nobackup" ];
          exclude_nodump = true;
        };
        storage = {
          encryption_passcommand = "cat ${home}/encryption_passphrase";
        };
        retention = {
          keep_daily = 10;
          keep_weekly = 6;
          keep_monthly = 18;
          keep_yearly = 5;
        };
        consistency = {
          checks = [ "repository" ];
        };
      };
    };

    systemd.services.borgmatic = {
      serviceConfig.WorkingDirectory = home;
    };

    systemd.timers.borgmatic = {
      timerConfig.RandomizedDelaySec = 14400;
      wantedBy = [ "timers.target" ];
    };

    users.users.borg = {
      inherit home;
      createHome = true;
      isSystemUser = true;
      group = "borg";
    };

    users.groups.borg = {};
  };
}
