{ pkgs, lib, config, ... }:

let
  website = config.luhj.website.home;
in
{
  imports = [ ./borgmatic.nix ];

  config = {
    services.borgmatic.settings = {
      location.source_directories = [ "/" ];
      hooks.before_backup = [''
        rm -f ${website}/data/db_dump/borg.sql
        ${website}/docker/dump_db.sh ${website}/data/db_dump/borg.sql
      ''];
    };
  };
}
