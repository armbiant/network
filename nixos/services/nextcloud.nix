{ pkgs, ... }:
{
  services.nextcloud = {
    enable = true;
    hostName = "cloud.luhj.de";
    https = true;
    package = pkgs.nextcloud25;
    caching.redis = true;
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      adminpassFile = "/var/lib/nextcloud/admin-password";
      adminuser = "ncadmin";
      defaultPhoneRegion = "DE";
    };
    # extraOptions = {
    #   redis = {
    #      host = "/run/redis/redis.sock";
    #      port = 0;
    #      dbindex = 1;
    #   };
    # };
    enableBrokenCiphersForSSE = false;
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
     { name = "nextcloud";
       ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
     }
    ];
  };

  services.nginx.virtualHosts."cloud.luhj.de" = {
    enableACME = true;
    forceSSL = true;
  };

  services.redis.servers."".enable = true;

  # ensure that postgres is running *before* running the setup
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };
}
