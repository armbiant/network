# Silverstripe web site for luhj.de and related domains.
# Clone the contents of git@gitlab.com:lutherhaus/homepage/docker.nix together
# with submodules into /srv/website/docker.

{ pkgs, lib, config, ... }:

let
  domains = [
    "luhj.de"
    "aufwind-gottesdienst.de"
    "kirche-wenigenjena.de"
    "lutherhaus-jena.de"
    "marienkirche-jena.de"
    "marienkirche-ziegenhain.de"
    "minornatu.de"
    "schiller-kirche.de"
    "schillerkirche.de"
  ];

  allDomains = domains ++ (map (d: "www.${d}") domains);

  home = config.luhj.website.home;

  websiteEnv = builtins.toFile "website_bash_profile" ''
    export SILVERSTRIPE_ASSETS_DIR_ON_HOST=${home}/data/assets
    export SILVERSTRIPE_LOG_DIR_ON_HOST=${home}/log
    export DB_DUMP_DIR=${home}/data/db_dump

    if [[ -e .bashrc ]]; then
      . .bashrc
    fi
  '';

  # "luhj.de".extraConfig = ''
  #   redir https://www.luhj.de{uri} temporary
  # '';
  wwwRedirects = lib.listToAttrs (map (domain: { name = domain; value = {
    extraConfig = ''
      redir https://www.${domain}{uri} temporary
    '';
  }; }) domains);

in {
  imports = [
    ./docker.nix
  ];

  options = with lib; {
    luhj.website.home = mkOption {
      type = types.path;
      default = "/srv/website";
      description = "base path for website";
    };
  };

  config = {
    # ensure web server startup regardless of DNS entries
    networking.hosts = { "127.0.0.1" = allDomains; };

    networking.firewall.allowedTCPPorts = [ 80 443 ];

    users.users.website = {
      inherit home;
      description = "Clone website repo into ${home}/docker";
      isNormalUser = true;
      group = "website";
      extraGroups = [ "docker" ];
      uid = 1006;
    };

    users.groups.website.gid = 1006;

    system.activationScripts.website_bash_profile.text = ''
      ln -sfn ${websiteEnv} ${home}/.bash_profile
      install -o website -g website -d ${home} ${home}/{docker,data,log}
    '';

    services.caddy = {
      enable = true;
      email = "admin@lutherhaus-jena.de";
      globalConfig = ''
        skip_install_trust
        local_certs
      '';
      virtualHosts = {
        "test.luhj.de".extraConfig = ''
          reverse_proxy localhost:8080
        '';
        "www.lutherhaus-jena.de".extraConfig = ''
          redir / /lutherhaus/ temporary
          reverse_proxy localhost:8080
        '';
        "www.schillerkirche.de".extraConfig = ''
          redir / /schillerkirche/ temporary
          reverse_proxy localhost:8080
        '';
        "www.marienkirche-ziegenhain.de".extraConfig = ''
          redir / /marienkirche/ temporary
          reverse_proxy localhost:8080
        '';
        "www.kirche-wenigenjena.de".extraConfig = ''
          redir / /gemeindebezirk/ temporary
          reverse_proxy localhost:8080
        '';
      } // wwwRedirects;
    };
  };
}
