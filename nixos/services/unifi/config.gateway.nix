{
  interfaces.tunnel.tun0 = {
    address = "2001:470:1f0a:2f0::2/64";
    description = "tunnelbroker.net IPv6";
    encapsulation = "sit";
    local-ip = "0.0.0.0";
    multicast = "disable";
    remote-ip = "216.66.80.30";
    mtu = 1472;
    firewall = {
      "in" = {
        ipv6-name = "WANv6_IN";
        name = "WAN_IN";
      };
      local = {
        ipv6-name = "WANv6_LOCAL";
        name = "WAN_LOCAL";
      };
      out = {
        ipv6-name = "WANv6_OUT";
        name = "WAN_OUT";
      };
    };
  };
  protocols.static.interface-route6."::/0".next-hop-interface = "tun0";
  service.dhcp-server.shared-network-name = {
    "net_MGM_eth1_10.2.1.0-24".subnet."10.2.1.0/24".static-mapping = {
      "bodenstein.mgm" = {
        ip-address = "10.2.1.9";
        mac-address = "08:60:6e:69:9e:e6";
      };
    };
    "net_INT_eth1_10.2.2.0-24".subnet."10.2.2.0/24".static-mapping = {
      "bodenstein" = {
        ip-address = "10.2.2.9";
        mac-address = "08:60:6e:69:9e:e6";
      };
      "COMpact_VoIP" = {
        ip-address = "10.2.2.10";
        mac-address = "00:09:52:02:31:8d";
      };
      "TA-MFP" = {
        ip-address = "10.2.2.11";
        mac-address = "00:17:c8:2d:72:a7";
      };
    };
    "net_SL_eth1_10.2.4.0-24".subnet."10.2.4.0/24".static-mapping = {
      "SQ-6" = {
        ip-address = "10.2.4.4";
        mac-address = "00:04:c4:05:f2:fe";
      };
      "ha" = {
        ip-address = "10.2.4.5";
        mac-address = "90:1b:0e:2e:7f:22";
      };
      "audiorec" = {
        ip-address = "10.2.4.6";
        mac-address = "4c:cc:6a:11:d3:aa";
      };
      "aio" = {
        ip-address = "10.2.4.7";
        mac-address = "04:0e:3c:c4:2b:35";
      };
      "sl4" = {
        ip-address = "10.2.4.8";
        mac-address = "b4:2e:99:a5:cb:60";
      };
      "bodenstein.sl" = {
        ip-address = "10.2.4.9";
        mac-address = "08:60:6e:69:9e:e6";
      };
      "lightshark" = {
        ip-address = "10.2.4.10";
        mac-address = "1e:4f:46:d2:3b:7c";
      };
      "pt20x-zcam.luhj.de" = {
        ip-address = "10.2.4.11";
        mac-address = "d4:e0:8e:7f:19:7f";
      };
      "shelly-e-ks-vent" = {
        ip-address = "10.2.4.21";
        mac-address = "e8:db:84:aa:6b:f5";
      };
      "shelly-e-ks-led-l" = {
        ip-address = "10.2.4.22";
        mac-address = "d8:bf:c0:51:d5:bc";
      };
      "shelly-e-ks-led-r" = {
        ip-address = "10.2.4.23";
        mac-address = "e8:db:84:bb:00:bb";
      };
      "shelly-e-ks-stern" = {
        ip-address = "10.2.4.24";
        mac-address = "3c:61:05:ef:6a:0c";
      };
    };
  };
  system.static-host-mapping.host-name = {
    # MGM
    "melanchton.mgm.luhj.de" = {
      inet = "10.2.1.5";
    };
    # INT
    "melanchton.luhj.de" = {
      inet = "10.2.2.5";
    };
    "pbx.luhj.de" = {
      alias = "pbx";
      inet = "10.2.2.10";
    };
    # SL
    "melanchton.sl.luhj.de" = {
      alias = "mqtt.luhj.de";
      inet = "10.2.4.5";
    };
    # IOT
    "melanchton.iot.luhj.de" = {
      alias = "mqtt.iot.luhj.de";
      inet = "10.2.5.5";
    };
  };
}
