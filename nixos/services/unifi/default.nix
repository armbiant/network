{ config, pkgs, lib, ... }:

with builtins;

let
  dataDir = "/srv/unifi";
  vhost = "unifi.luhj.de";

  # tunnelbroker needs ICMP accepted on WAN_LOCAL
  configGatewayJson = toFile "config.gateway.json" (
    toJSON (import ./config.gateway.nix));

  unifiCompose = toFile "docker-compose.yml" (
    import ./docker-compose.nix { inherit config dataDir; });

  unifiLogs = "${dataDir}/config/logs";

  unifiSvc = rec {
    script = "docker-compose up --abort-on-container-exit";
    preStart = "ln -fs ${unifiCompose} ${dataDir}/docker-compose.yml";
    path = with pkgs; [ docker-compose ];
    after = [ "docker.service" "network.target" ];
    requires = after;
    environment = { COMPOSE_FILE = unifiCompose; };
    serviceConfig = {
      Group = "unifi";
      User = "unifi";
      Restart = "on-failure";
      WorkingDirectory = dataDir;
    };
    wantedBy = [ "multi-user.target" ];
  };

in {
  imports = [ ../docker.nix ];

  config = {
    # https://help.ubnt.com/hc/en-us/articles/218506997-UniFi-Ports-Used
    networking.firewall = {
      allowedUDPPorts = [ 3478 10001 1900 ];
      allowedTCPPorts = [ 80 443 8080 8443 8880 8843 6789 ];
    };

    security.acme.certs.${vhost}.email = "admin@lutherhaus-jena.de";

    services.logrotate.settings.unifi = {
      files = [ "${unifiLogs}/*.log" ];
      create = "0644 unifi unifi";
      daily = true;
      rotate = 14;
    };

    services.nginx = {
      enable = true;
      recommendedGzipSettings = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      recommendedOptimisation = true;
      virtualHosts.${vhost} = {
        default = true;
        enableACME = true;
        addSSL = true;
        # see https://blog.ljdelight.com/nginx-proxy-to-ubiquiti-unifi-controller/
        locations."/" = {
          proxyPass = "https://localhost:8443";
          extraConfig = ''
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
          '';
        };
        root = "${dataDir}/www";
      };
    };

    system.activationScripts.unifi = lib.stringAfter [ "users" ] ''
      install -D -o unifi -g unifi -m 0644 \
        ${configGatewayJson} \
        ${dataDir}/config/data/sites/default/config.gateway.json
    '';

    systemd.services.unifi-docker = unifiSvc;

    systemd.tmpfiles.rules = [
      "d ${dataDir} 755 unifi unifi"
      "d ${unifiLogs} 755 unifi unifi"
    ];

    users = {
      groups.unifi.gid = 401;

      users = {
        unifi = {
          description = "UniFi Controller";
          extraGroups = [ "docker" ];
          group = "unifi";
          home = dataDir;
          isSystemUser = true;
          uid = 401;
        };
      };
    };
  };
}
