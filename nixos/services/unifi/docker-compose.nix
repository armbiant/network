{ config, dataDir }:

with config.users;

''
  version: '3'
  services:
    unifi:
      image: linuxserver/unifi-controller:7.3.76
      container_name: unifi
      environment:
        - TZ=Europe/Berlin
        - PUID=${toString users.unifi.uid}
        - PGID=${toString groups.unifi.gid}
        - MEM_LIMIT=512
      volumes:
        - ${dataDir}/config:/config
      network_mode: "host"
''
