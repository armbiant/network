{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ mosquitto ];

  networking.firewall.allowedTCPPorts = [ 1883 ];

  services.mosquitto = {
    enable = true;
    listeners = [ {
      acl = [
        "topic read $SYS/#"
        "pattern write $SYS/broker/connection/%c/state"
      ];
      users = {
        "shelly-ventilation" = {
          acl = [ "shellies/#" "homeassistant/#" ];
          hashedPassword =
            "$7$101$bQ/Dyb435EOAjMIE$N6F1ocTdQppH8WRYadstCvp0x3yrc/WCUmY95+ER7FwWd2s8+xydH4xBDOo92Zv/j+KGXCxp7iKWZJiABw4sEw==";
        };
        "tallylight" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$cy+CYfemR6oOjYV6$wVxGFm8hf+6JzxK+TS6QIT/mJQ7St6DBJQDsFHRisSGeS2aOF60F2/cIFiMq7gm1q0p2pX5ke5VV5ILRK+md6Q==";
        };
        "obs" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$3ZtvDy39cC6YpHsk$YhgVQi8wTYsJinhFof3tuWqfBi9Lnzr9uIV/EW5e0dCBu75cwgjoYBIC1bNCNNqzm6Nl63vaQpnlPZTohnpBrw==";
        };
        "onair" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$r/Ft4kuRktFNDWeg$cj864KQMjiqY47Vvm34J9EWLbq0yTtyYUDTaPa0IzEilrk4zgrbdH8/naSRp5Ntjk0rkrf1J0SdrUohFn30T8w==";
        };
        "tally-totale" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$U+iB+hhqroYxGKvT$oXH2eJH16VOTTHu4ODKQ4r0SJshh/pDsI0FwtA1RRk0c1egDEeNL9hG6i0DehbjSd2mtTjUNAgFFdu0d6p/ddw==";
        };
        "hass" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$vkSMIiBsBfaGltof$kGa/JDWCM4HJ+uBRdIFCOPzOaLiTlpBmGMlKHkv/2ixggEP1G8BDkxzysKBKIwWjTuGil8SSmPDflMsxt/1TZQ==";
        };
        "tally3" = {
          acl = [ "tally3/#" "homeassistant/#" ];
          hashedPassword =  # SisUvTacoon4
            "$7$101$dJ0Dk8fSJISMgnVO$GTzDwxf/GowZykCG8rsB6Obp4B2ZaI5e4NUd6HiYkvdHgZ8xYErWPK+ZxNPB7jdN30G7AIs1ekymiXFRIJHvWQ==";
        };
        "tally4" = {
          acl = [ "tally4/#" "homeassistant/#" ];
          hashedPassword =  # SisUvTacoon4
            "$7$101$dJ0Dk8fSJISMgnVO$GTzDwxf/GowZykCG8rsB6Obp4B2ZaI5e4NUd6HiYkvdHgZ8xYErWPK+ZxNPB7jdN30G7AIs1ekymiXFRIJHvWQ==";
        };
        "BSB-LAN" = {
          acl = [ "BSB-LAN/#" ];
          hashedPassword =
            "$7$101$UQCi9kbrSriE7tfQ$37kfliLMtm5NtOVYrDU6B7r1z5l0LmWqB8xQ/nkDfxmImDd78l+j9sYGxU7TEsk703MjUVHPHhPuYKl23VXkQw==";
        };
      };
    } ];
  };
}
