# ChurchTools users via LDAP

{ pkgs, ... }:
{
  config = {
    services.openldap = with pkgs; {
      enable = true;
      settings = {
        children = {
          "cn=schema".includes = [
            "${openldap}/etc/schema/core.ldif"
            "${openldap}/etc/schema/cosine.ldif"
            "${openldap}/etc/schema/inetorgperson.ldif"
            "${openldap}/etc/schema/misc.ldif"
          ];
          "olcDatabase={1}mdb".attrs = {
            objectClass = [ "olcDatabaseConfig" "olcMdbConfig" ];
            olcDatabase = "{1}mdb";
            olcDbDirectory = "/var/db/openldap";
            olcDbIndex = [
              "objectClass eq"
              "cn,uid,mail pres,eq"
              "sn pres,eq,subany"
            ];
            olcSuffix = "dc=luhj,dc=de";
            olcRootDN = "cn=manager,dc=luhj,dc=de";
            olcRootPW = "{SSHA}Daj85x40XY1ZDK6DCtqXFgG8BNcGTfyr";
          };
        };
      };
    };

    environment.etc."openldap/init.ldif".text = ''
      dn: dc=luhj,dc=de
      dc: luhj
      objectClass: organizationalUnit
      objectClass: dcObject
      ou: Lutherhaus Jena

      dn: ou=users,dc=luhj,dc=de
      objectClass: organizationalUnit
      ou: users
    '';
  };
}
