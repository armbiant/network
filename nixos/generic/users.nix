{ ... }:
{
  users.groups = {
    produktion.gid = 1005;
  };

  users.users = {
    bastian = {
      description = "Bastian Harendt";
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" ];
      uid = 1000;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyWxci4MoFGH/qXGmuKosvtOhgzFjzjcIZfqBHE/4G3dAFZJqaYERPeXmH6Qn69aX9iHrCKX7pS9MelzzUOhrUYPbcHolwO68vmzUHefgZRWvghHhFYYRQ2Cxizykhgj2C5ub0cqKXl14eu5VhT9D2WfV3faDU+veogo5vR35RjhSVbC7ZhF7pGnyie3NA670Yh+LtMRhNEuGc4VE7GD5fvH7ScaJW8Firf7Fblglhdpiir1LeZKMmGe8cHdFtsQInsdUwkgJR0Eicz94Us8bAwOqsqTW5E/BRoKPRVgeMu/acPuIAkEKTm5vimX6VUPY1XKnD4j+voQFbrgE/x6ziLwY5j22dg/Yx5OPw8X5cOGygmx2moRqpqD4N5h/sgUaXPdparSPBJrO1BmUrMqgD7seudVz95zu5Rr8L2oyqYDUosarY3qmqSNEK926IC19/MnTDEgf7/9SXRk3pV2fnGSAwAu+H692iKJc4ooXKllxpyXfdBxr0FojOwW4/SiqNvlkR7FEbJF4ZaVIa/EYp/YP7Fnxmhy2mqPHFvqllt0MKMtPI0PoqOZ9TeX1Yp8Y4bLSp5I2QA5oFi49djUZ8G4LVL8NVu8DDylSpiC6lZq2oxa48nVCF1L3FuxNvZcpqdN87gTBPKoujNmv7S3EoRijbv2NzKEZ+9qKe7jNnuw== bastian@braavos"
      ];
    };
    mm = {
      description = "Markus Mueller";
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      uid = 1001;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8+q4yNZkrjkFnj29sjCB/cdYO3mxKL4qXKfcflAg0YJxxqj1MPutQiwg7cu/9WR/yvIxRWQ1hdtBl4y1upi3IEsyqUgeV4PvdGwAPjCecPE8Gp+kj71Kx282YQ3dSjJXCZKT1MNSqrrrWCqfYhhl/izHeHZxaqE3Eg7qbpwwNzOZ9FJq4QfHBPksOCxmzsXMCOTL+XVsAPmIyEU8bIUcrmDW1vJIVD4W/Py+r/YIUY39vG/uBFDqQsdZAs6kv5I3PKz0aUQfMdioQg/BjSl85tCtY8XUs5FVix5RT5T0ZM56AVZCcUFwC/fbwJwY2dRZKxs6u/yp07cNVU6lAZcFr mm@Georgia"
      ];
    };
    fgehring = {
      description = "Friedrich Gehring";
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      uid = 1002;
    };
    ck = {
      description = "Christian Kauhaus";
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" ];
      uid = 1003;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCz7v7YuSEuG4R8Lq6UL6xWB2ew5UMp8zdijRbX8uao9gvMNWbvhbfFE6O8/Pp27H8Hneeqe9nOh/AxeIP+YJU3AM74QWhvqgP/emHThvUzPc9pOILwrLCgTCf8gXzR5Rvsor4XIxNNZj5BKXo71GLbpKBVYWiBNW1Z5YG4pAbcAldWS1LK6zdj6cc94D2Y4S2CGNfafJNW0BG+E0rdTvpoJPfiLMP0b0J+M2SM7fJRUHUZgeYZux44NPP74NC5YcKF7ejoHqiUy4H09Be5ukNhIRuSjeM0dlmiUfAcKndcLA6EuomDZvXCACc7l6Ukn5tC1sSrw7l7VGK0DC65A459 ckauhaus@lionel"
      ];
    };
    cg = {
      description = "Clemens Graetz";
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" ];
      uid = 1004;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEj9Q1gwK4oDfIcKITmvHyjBjC1iYmr5GCLer8zNSg/7Ex67KAcxaxAVLFqL0T7Eljx/FNOZbjvwzpBaxqsbpiffo6m0FX6GQxwfe75FKsxq+f7iOA6J/2nG2RtgruQ2sA44xd6BtWD2M/2QP1FoP/PmeMI2FcxV91zHhveSGDELe3mykFVSicX62VRbGvWYloMSzWqkt2JE6pMypWZkcz8rhegS5S7C6+rXCYEyQ9hH4bVYkVKoEMKf7K4nSxk+MFSSO6EQ27n/jsGUWdEEyjwA0HmdhDkZag40gA48WyQ+efBWPZ58tTHWf7tXSQUQHYfNGhWVCsOND4pGamUn+VXtX9tOHiHJ+GgQzfSV6mEIgVDrNeXJlXIEPRtRGZ0KJUtssOA4sShBr396ZDecmS2LoiVKk+v4G3qVoNt8LG/VIE4p7dBxsjdsSSVXMuh3qs54p+VomZy8zZlheTBdWtpr34QUr5dguF163zqp+7Kf7NAcYY106L6Rr7venM+pc= space@Peter-PC"
      ];
    };
    technikteam = {
      description = "Produktionsteam";
      isNormalUser = true;
      extraGroups = [ ];
      uid = 1005;
      group = "produktion";
    };
  };

  # UID/GID 1006: website
}
