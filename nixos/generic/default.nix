# Generic settings for all hosts managed by us
# Specific settings can be found in hosts/$HOSTNAME.nix
{ config, pkgs, lib, ... }:

let
  systemUpdate = ''
    touch /var/lib/nixos-system-update.started
    echo "* Update nixos configuration"
    cd /root/network
    git pull
    echo "* Collect garbage"
    nix-collect-garbage --delete-older-than 3d --max-freed 1G
    echo "* Optimize store"
    nix-store --optimize
    sleep 5
    echo "* Rebuild system"
    nixos-rebuild --upgrade switch
    sleep 5
    echo "* fstrim "
    fstrim -a
    sleep 5
    rm -f /var/lib/nixos-system-update.started
    sync
    currentKernel=$(readlink /run/current-system/kernel)
    bootedKernel=$(readlink /run/booted-system/kernel)
    if [[ "$currentKernel" != "$bootedKernel" ]]; then
      echo "* Kernel has been updated, rebooting"
      echo "$bootedKernel -> $currentKernel"
      sleep 5
      reboot
    fi
  '';

in
{
  imports = [ ./users.nix ];

  config = {
    boot.cleanTmpDir = true;
    documentation.nixos.enable = false;

    environment.interactiveShellInit = ''
      TMOUT=86400
    '';

    environment.systemPackages = with pkgs; [
      apg
      atop
      bc
      file
      git
      gptfdisk
      inetutils
      jq
      lsof
      mailutils
      mkpasswd
      mmv
      netcat
      parted
      psmisc
      python3
      ripgrep
      sysstat
      tcpdump
      tmux
      tree
      unzip
      vim
      wget
      zip
    ];

    i18n.defaultLocale = "en_US.UTF-8";

    networking = {
      firewall.rejectPackets = true;
      tempAddresses = "disabled";
      domain = "luhj.de";
    };

    programs.vim.defaultEditor = true;

    services = {
      journald.extraConfig = "SystemMaxUse=512M";
      openssh.enable = true;

      logrotate = {
        enable = true;
        settings = {
          default = {
            global = true;
            priority = 10;
            dateext = true;
            compress = true;
            delaycompress = true;
          };
          lastlog = {
            files = [ "/var/log/lastlog" ];
            create = "0644 root root";
          };
        };
      };
    };

    sound.enable = false;

    systemd.services = {
      nixos-system-update = {
        script = systemUpdate;
        description = "Update NixOS and clean up store";
        startAt = "*-*-3/7 01:40";
        path = with pkgs; [
          utillinux nix config.system.build.nixos-rebuild git
        ];
        environment.NIX_PATH =
          "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos:" +
          "nixos-config=/etc/nixos/configuration.nix";
        restartIfChanged = false;
        stopIfChanged = false;
        serviceConfig = {
          Type = "oneshot";
          WorkingDirectory = "/root";
        };
      };
    };

    systemd.tmpfiles.rules = [
      "d /srv 755"
    ];

    time.timeZone = "Europe/Berlin";
    xdg.mime.enable = false;
  };
}
