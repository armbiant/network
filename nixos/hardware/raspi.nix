# Settings specific to Raspberry Pi 3
{ pkgs, ... }:
{
  boot = {
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
      raspberryPi = {
        enable = true;
        version = 3;
      };
    };
    kernelParams = [
      "cma=32M"
      "console=ttyS1,115200n8" "console=tty0"
    ];
    initrd.availableKernelModules = [
      # Allows early (earlier) modesetting for the Raspberry Pi
      "vc4" "bcm2835_dma" "i2c_bcm2835"
    ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS";
      fsType = "xfs";
    };
    "/boot" = {
      device = "/dev/disk/by-label/BOOT";
      fsType = "ext4";
    };
  };

  # needed to get Wifi chipset working
  hardware.enableRedistributableFirmware = true;

  networking.dhcpcd.allowInterfaces = [ "eth0" "wlan0" ];

  # not necessary hardware-dependent, but we don't require passwords for all
  # users on embedded systems
  security.sudo.wheelNeedsPassword = false;

  zramSwap.enable = true;
  zramSwap.memoryPercent = 25;
}
