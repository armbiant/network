# Long-lived VM which runs website and mailing list services.
# This is intended as a generic image running on an external hosting provider.

{ pkgs, config, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
    ../generic
    ../services/borgbackup/calvin.nix
    ../services/website.nix
  ];

  config = {
    boot = {
      growPartition = true;
      kernelParams = [ "console=ttyS0" ];
      loader.grub.device = "/dev/sda";
    };

    fileSystems = {
      "/" = {
        device = "/dev/disk/by-label/nixos";
        autoResize = true;
        fsType = "ext4";
      };
    };

    networking = {
      hostName = "calvin";
      hostId = "67bf68f4";

      defaultGateway = { address = "5.9.54.69"; };
      defaultGateway6 = { address = "2a01:4f8:161:504e::2"; };

      interfaces.eth0 = {
        ipv4.addresses = [ {
          address = "5.9.54.89";
          prefixLength = 27;
        } ];
        ipv6.addresses = [ {
          address = "2a01:4f8:161:504e::4";
          prefixLength = 64;
        } ];
      };

      nameservers = [
        "185.12.64.1"
        "2a01:4ff:ff00::add:1"
        "185.12.62.2"
        "2a01:4ff:ff00::add:2"
      ];

      useDHCP = false;
    };

    system.stateVersion = "21.11";
  };
}
