{ pkgs, ... }:
{
  imports = [
    ../generic
    ../hardware/melanchton.nix
    ../site/luhj.nix
    ../services/borgbackup/melanchton.nix
    ../services/homeassistant
    ../services/nextcloud.nix
    ../services/nfs.nix
    ../services/unifi
  ];

  config = {
    environment.systemPackages = with pkgs; [
      zfstools
    ];

    networking = {
      hostName = "melanchton";
      hostId = "23b1a2d4"; # needed for zfs

      bonds.bond0 = {
        interfaces = [ "eno1" "enp8s5" ];
        driverOptions = {
          mode = "802.3ad";
          lacp_rate = "fast";
        };
      };

      hosts = {
        # needed for NFS export
        "10.2.4.6" = [ "audiorec.luhj.de" "audiorec" ];
        "10.2.4.8" = [ "sl4.luhj.de" "sl4" ];
      };

      # see also services/unifi/config.gateway.nix for static addresses
      interfaces = {
        # cannot use DHCP here because we run the Unifi controller
        bond0.ipv4.addresses = [ { address = "10.2.1.5"; prefixLength = 24; } ];
        int.ipv4.addresses = [ { address = "10.2.2.5"; prefixLength = 24; } ];
        # MQTT, NFS, HA
        sl.ipv4.addresses = [ { address = "10.2.4.5"; prefixLength = 24; } ];
        iot.ipv4.addresses = [ { address = "10.2.5.5"; prefixLength = 24; } ];
      };
    };

    services.nfs.server.exports = ''
      /srv/produktion/ardour audiorec(rw,no_subtree_check)
      /srv/produktion/video sl4(rw,no_subtree_check)
    '';

    services.zfs.autoScrub.enable = true;

    system.stateVersion = "20.09";
  };
}
