# nix-shell -p nixos-generators \
#   --run 'nixos-generate -f vagrant-virtualbox -c calvin.nix
{ pkgs, lib, config, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/virtualisation/vagrant-virtualbox-image.nix>
    <nixpkgs/nixos/modules/virtualisation/vagrant-guest.nix>
    ../generic
    ../services/website.nix
  ];

  config = {

    boot.loader.grub.device = "/dev/sda";
    boot.initrd.checkJournalingFS = false;

    fileSystems."/vagrant" = {
      fsType = "vboxsf";
      device = "vagrant";
      options = [ "ro" "uid=25274" "gid=100" ];
    };

    fileSystems."/srv/website" = {
      fsType = "vboxsf";
      device = "srv_website";
      options = [ "rw" "uid=1006" "gid=1006" ];
    };

    networking = {
      hostName = "calvin-vagrant";

      interfaces.enp0s8.ipv4.addresses = [ {
        address = "192.168.56.4";
        prefixLength = 24;
      } ];
    };

    services.caddy.globalConfig = ''
      local_certs
    '';

    system.stateVersion = "21.11";

    users = {
      mutableUsers = false;
      users.root.hashedPassword = "";
      users.vagrant.uid = lib.mkOverride 90 25274;
    };
  };
}
