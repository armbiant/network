# Web site develop / testing environment

## Set up the environment

Create `website` directory and clone
git@gitlab.com:lutherhaus/homepage/docker.git into `website/docker`.

## Create testing VM

1. Generate Vagrant box:

    nix-shell -p nixos-generators \
      --run 'nixos-generate -f vagrant-virtualbox -c calvin.nix'

2. Register box:

    vagrant box add --name nixbox /nix/store/...-virtualbox-vagrant.box

3. Start Vagrant and log in:

    vagrant up
    vagrant ssh

## Create test site

Inside the Vagrant box:

1. Use sudo to become 'website'
2. cd into /srv/website/docker
3. Follow the steps from README.md.

## Testing the web site

Edit /etc/hosts on the host machine (outside Vagrant) to include lines like

    192.168.56.4 www.luhj.de

All relevant domain names can be found in /etc/hosts inside the Vagrant VM.
